package com.shoppingcart.repository

import com.shoppingcart.entity.ProductEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository  : CrudRepository<ProductEntity, String> {
    override fun findAll():List<ProductEntity>;
}