package com.shoppingcart.repository

import com.shoppingcart.entity.ShoppingCartEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ShoppingCartRepository: CrudRepository<ShoppingCartEntity, String> {
    override fun findById(uuid: String): Optional<ShoppingCartEntity>;
}