package com.shoppingcart.entity

import com.shoppingcart.constants.ProductTypes
import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash

@RedisHash("product")
data class ProductEntity
    (@Id val uuid: String?,
     val name: String,
     val sku: String,
     val description: String,
     val type: ProductTypes,
     val price: Double)