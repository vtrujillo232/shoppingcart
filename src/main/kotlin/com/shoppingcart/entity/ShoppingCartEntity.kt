package com.shoppingcart.entity

import com.shoppingcart.constants.ShoppingCartState
import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash

@RedisHash("shopping_cart")
data class ShoppingCartEntity (@Id val uuid: String,
                               var state: ShoppingCartState,
                               val products:MutableSet<String>)