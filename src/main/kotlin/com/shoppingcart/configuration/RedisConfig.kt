package com.shoppingcart.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.connection.RedisConnectionFactory
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory
import org.springframework.data.redis.core.RedisTemplate

@Configuration
class RedisConfig {

    @Bean
    fun connectionFactory(): RedisConnectionFactory {
        return JedisConnectionFactory();
    }

    @Bean
    fun redisTemplate(redisConnectionFactory:RedisConnectionFactory) : RedisTemplate<ByteArray, ByteArray>{
        val template:RedisTemplate<ByteArray, ByteArray> = RedisTemplate<ByteArray, ByteArray>();
        template.setConnectionFactory(redisConnectionFactory);
        return template;
    }

}