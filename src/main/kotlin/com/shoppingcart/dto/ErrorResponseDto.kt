package com.shoppingcart.dto

data class ErrorResponseDto (val message:String)