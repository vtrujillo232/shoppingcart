package com.shoppingcart.dto

import com.shoppingcart.constants.ProductTypes

data class ProductDto (val name: String,
                       val sku: String,
                       val description: String,
                       val type: ProductTypes,
                       val price: Double)