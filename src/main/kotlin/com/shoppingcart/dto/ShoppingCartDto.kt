package com.shoppingcart.dto

import com.shoppingcart.entity.ProductEntity

data class ShoppingCartDto(val uuid: String ,val products: List<ProductEntity>);