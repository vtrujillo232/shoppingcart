package com.shoppingcart.dto

data class CheckoutResponseDto(val total:Double)