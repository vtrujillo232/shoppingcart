package com.shoppingcart

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories

@SpringBootApplication
@EnableRedisRepositories
@EnableAutoConfiguration
class ShoppingCartApplication

fun main(args: Array<String>) {
	runApplication<ShoppingCartApplication>(*args)
}


