package com.shoppingcart.constants

enum class ShoppingCartState {
    PENDING, COMPLETE
}