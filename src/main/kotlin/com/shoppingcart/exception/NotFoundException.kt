package com.shoppingcart.exception

class NotFoundException(val msg:String) : Exception() {
}