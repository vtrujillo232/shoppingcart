package com.shoppingcart.exception

import com.shoppingcart.dto.ErrorResponseDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler


@ControllerAdvice
class ControllerExceptionsHandler {

    @ExceptionHandler(NotFoundException::class)
    fun notFoundExceptionHandling (exc:NotFoundException): ResponseEntity<ErrorResponseDto> {
        return ResponseEntity(ErrorResponseDto(exc.msg),HttpStatus.NOT_FOUND);
    }



}
