package com.shoppingcart.service

import com.shoppingcart.constants.ShoppingCartState
import com.shoppingcart.dto.CheckoutResponseDto
import com.shoppingcart.dto.ShoppingCartDto
import com.shoppingcart.entity.ProductEntity
import com.shoppingcart.entity.ShoppingCartEntity
import com.shoppingcart.exception.NotFoundException
import com.shoppingcart.repository.ShoppingCartRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class ShoppingCartService (private val shoppingCartRepository: ShoppingCartRepository, private val productService: ProductService){


    fun checkoutShoppingCart(uuidShoppingCart:String): CheckoutResponseDto {
        val shoppingCart:ShoppingCartDto = getShoppingCartDto(uuidShoppingCart)
        val shoppingCartEntity: ShoppingCartEntity = getShoppingCartById(uuidShoppingCart)
        shoppingCartEntity.state=ShoppingCartState.COMPLETE
        shoppingCartRepository.save(shoppingCartEntity)
        var total :Double = 0.0
        shoppingCart.products.forEach {
            total += it.price
        }
        return CheckoutResponseDto(total)
    }

    fun addProductToShoppingCart(uuidShoppingCart:String,uuidProduct:String):ShoppingCartDto{
        var shoppingCartEntity: ShoppingCartEntity = getShoppingCartById(uuidShoppingCart);
        shoppingCartEntity.products.add(uuidProduct)
        shoppingCartRepository.save(shoppingCartEntity)
        return getShoppingCartDto(uuidShoppingCart);
    }

    fun deleteProductFromShoppingCart(uuidShoppingCart:String,uuidProduct:String): ShoppingCartDto{
        var shoppingCartEntity: ShoppingCartEntity = getShoppingCartById(uuidShoppingCart);
        shoppingCartEntity.products.remove(uuidProduct)
        shoppingCartRepository.save(shoppingCartEntity)
        return getShoppingCartDto(uuidShoppingCart);
    }


    fun createShoppingCart(products:Set<String>):ShoppingCartDto{
        val productsSet:MutableSet<String> =  mutableSetOf<String>()
        productsSet.addAll(products)
        val id = UUID.randomUUID().toString()
        shoppingCartRepository.save(ShoppingCartEntity(id, ShoppingCartState.PENDING ,productsSet))
        return getShoppingCartDto(id)
    }

    fun modifyShoppingCart(uuidShoppingCart:String,products:Set<String>):ShoppingCartDto{
        var shoppingCartEntity: ShoppingCartEntity = getShoppingCartById(uuidShoppingCart)
        shoppingCartEntity.products.clear()
        shoppingCartEntity.products.addAll(products)
        shoppingCartRepository.save(shoppingCartEntity)
        return getShoppingCartDto(uuidShoppingCart)
    }


    fun getShoppingCartDto(uuid:String) : ShoppingCartDto {
           val cartProducts : MutableList<ProductEntity> = mutableListOf<ProductEntity>();
        val shoppingCartEntity: ShoppingCartEntity = getShoppingCartById(uuid);
            shoppingCartEntity.products.map { uuidProduct->
                cartProducts.add(productService.getProductById(uuidProduct))
            }
            return ShoppingCartDto(uuid,cartProducts);
    }


    private fun getShoppingCartById(uuid:String):ShoppingCartEntity{
        val shoppingCart: Optional<ShoppingCartEntity> =  shoppingCartRepository.findById(uuid);
        if (!shoppingCart.isPresent)
            throw NotFoundException("Shopping Cart $uuid not found");
        else
            return shoppingCart.get();
    }

}