package com.shoppingcart.service

import com.shoppingcart.constants.ProductTypes
import com.shoppingcart.dto.ProductDto
import com.shoppingcart.repository.ProductRepository
import com.shoppingcart.entity.ProductEntity
import com.shoppingcart.exception.NotFoundException
import org.springframework.stereotype.Service
import java.util.*


@Service
class ProductService (val productRepository: ProductRepository) {

    fun getAllProducts(): List <ProductEntity>? {
        return productRepository.findAll();
    }

    fun saveProduct(productDto: ProductDto): ProductEntity {
        return productRepository.save(ProductEntity(UUID.randomUUID().toString(),
            productDto.name,
            productDto.sku,
            productDto.description,
            productDto.type,
            if(productDto.type== ProductTypes.NORMAL)
                productDto.price else  productDto.price/2));
    }

    fun editProduct(productDto: ProductDto, id: String): ProductEntity {
        return productRepository.save(ProductEntity(id,
            productDto.name,
            productDto.sku,
            productDto.description,
            productDto.type,
            if(productDto.type== ProductTypes.NORMAL)
                productDto.price else  productDto.price/2));
    }

    fun deleteProduct(id: String): String{
        productRepository.deleteById(id);
        return id;
    }

    fun getProductById(id: String): ProductEntity {
        val product:Optional<ProductEntity>  = productRepository.findById(id)
        if(!product.isPresent)
            throw NotFoundException("Product $id not found");
        else
        return productRepository.findById(id).get();
    }


}