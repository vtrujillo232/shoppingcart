package com.shoppingcart.controller

import com.shoppingcart.dto.ProductDto
import com.shoppingcart.service.ProductService
import com.shoppingcart.entity.ProductEntity
import org.springframework.web.bind.annotation.*

@RestController
class ProductController (val productService: ProductService){
    @GetMapping("/products")
    fun getAllProducts(): List <ProductEntity>? {
        return productService.getAllProducts();
    }

    @PostMapping("/products")
    fun saveProduct(@RequestBody productDto: ProductDto): ProductEntity {
        return productService.saveProduct(productDto);
    }

    @PatchMapping("/products/{id}")
    fun editProduct(@RequestBody productDto: ProductDto, @PathVariable id: String): ProductEntity {
        return productService.editProduct(productDto,id);
    }

    @DeleteMapping("/products/{id}")
    fun deleteProduct(@PathVariable id: String): String{
        return productService.deleteProduct(id);
    }


}