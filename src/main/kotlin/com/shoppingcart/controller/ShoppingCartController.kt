package com.shoppingcart.controller

import com.shoppingcart.dto.CheckoutResponseDto
import com.shoppingcart.dto.ShoppingCartDto
import com.shoppingcart.service.ShoppingCartService
import org.springframework.web.bind.annotation.*

@RestController
class ShoppingCartController (val shoppingCartService: ShoppingCartService) {

    @GetMapping("/shopping_carts/{uuid}")
    fun getShoppingCart(@PathVariable uuid:String):ShoppingCartDto{
        return shoppingCartService.getShoppingCartDto(uuid)
    }


    @PutMapping("/shopping_carts/{uuidShoppingCart}/products/{uuidProduct}")
    fun addProductToShoppingCart(@PathVariable uuidShoppingCart:String,@PathVariable uuidProduct:String):ShoppingCartDto {
        return shoppingCartService.addProductToShoppingCart(uuidShoppingCart,uuidProduct)
    }

    @DeleteMapping("/shopping_carts/{uuidShoppingCart}/products/{uuidProduct}")
    fun deleteProductFromShoppingCart(@PathVariable uuidShoppingCart:String,@PathVariable uuidProduct:String): ShoppingCartDto{
        return shoppingCartService.deleteProductFromShoppingCart(uuidShoppingCart,uuidProduct)
    }


    @PostMapping("/shopping_carts/")
    fun createShoppingCart(@RequestBody products:Set<String>):ShoppingCartDto{
        return shoppingCartService.createShoppingCart(products)
    }


    @PatchMapping("/shopping_carts/{uuidShoppingCart}")
    fun modifyShoppingCart(@PathVariable uuidShoppingCart:String,@RequestBody products:Set<String>):ShoppingCartDto{
        return shoppingCartService.modifyShoppingCart(uuidShoppingCart,products)
    }

    @PutMapping("/shopping_carts/checkout/{uuidShoppingCart}")
    fun checkoutShoppingCart(@PathVariable uuidShoppingCart:String): CheckoutResponseDto {
        return shoppingCartService.checkoutShoppingCart(uuidShoppingCart)
    }









}