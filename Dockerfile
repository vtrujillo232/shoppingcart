FROM java:8
VOLUME /tmp
ADD build/libs/shoppingcart-0.0.1-SNAPSHOT.jar shoppingcart-0.0.1-SNAPSHOT.jar
RUN bash -c 'touch /shoppingcart-0.0.1-SNAPSHOT.jar'
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/shoppingcart-0.0.1-SNAPSHOT.jar"]